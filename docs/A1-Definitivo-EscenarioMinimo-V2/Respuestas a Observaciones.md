# Respuestas a las Observaciones Generales

##1. Quitar Plataforma MOOC
Perfecto. La quitamos. Pero quedamos en manos de los tiempos del CNTI para establecer la estrategia formativa. Aún esta plataforma no está activa, y la estimación de su activación es para finales de Noviembres. Sin embargo, en el corto plazo parece ser una buena idea para reducir costos, pero en el largo plazo constituye un riesgo enorme dado que dependemos de la direccionalidad política y la voluntad de quien dirija esa institución.
Nuestra propuesta va hacia un enfoque productivo donde una de nuestras Unidades Productivas asuma esta plataforma y se autosustente con los ingresos provenientes de ella. Confiamos en el dinamismo que podrá darle la unidad productiva que asuma esto y que se dedique y especialice en el área formativa, tanto en lo relacionado con el PRO Nacional como en otras tecnologías pertinentes. De cualquier forma se mantiene la estrategia de dedicar una de las unidades productivas exclusivamente a la formación.

##2. Dividir en Fases
Mantenemos una sola fase. Es muy complejo dividirlo en fases, debido al carácter monolítico de los procesos del estado. Aún cuando son módulos, todos deben estar disponible para que la plataforma ofrezca la solución ideal. La cantidad de personas es la mínima para alcanzar los objetivos.

##3. ¿Donde estarán Ubicadas estas Unidades Productivas?
* 3 Área Metropolitana
* 1 Mérida
* 1 Lara

##4. ¿Qué figura jurídica tendrán estas Unidades Productivas?
Hasta el momento contamos con:

* Fundación ATTA (J-408622610) (Fundación)
* Empero C.A. (J-408623170) (Compañía Anómima)
* Saní Tecnologías Comunes (J-404394974) (Asociación Cooperativa)
* Por definir
* Por definir

Además, cualquier otra que contemple nuestro código de comercio (Compañía Anónima, Sociedad Anónima, Cooperativa, Asociación Civil, Fundación) o nuestra ley del Sistema Económico Comunal (Empresa de Propiedad Social Directa y Empresa de Propiedad Social Indirecta).
Es importante destacar que entre los objetivos del proyecto es acompañar en los nuevos emprendimientos a que alcancen la constitución legal de sus unidades productivas. En primera instancia no pretendemos encausar la conformación de un solo tipo de figura jurídica por considerar que debe ser decisión de cada unidad productiva luego del debate y consenso de sus integrantes con el acompañamiento de un equipo promotor que asesore al respecto.

##5. Una metodología
¿A qué nos referimos con “una metodología”: Este proyecto implica hacer cosas de una forma en que no se han hecho antes, innovar, sin errar. Al estar innovando es indispensable dejar un registro organizado y sistematizado de manera que pueda ser replicado fácilmente en otras oportunidades por actores diferentes. Una metodología debe responder principalmente el “quien, cómo, cuando”. Una propuesta metodológica es un instrumento de altísimo valor agregado que debe disponer de recursos propios para contratar un equipo especializado para la sistematización de la experiencia y la creación de los documentos. Si este objetivo (la metodología) no se contempla, se corre el riesgo tener éxito y no poder repetir el éxito; o tener fracaso y no saber cómo evitar nuevos fracasos.
Sin embargo, debido a la solicitud de reducción de costos, decidimos retirar la metodología de los productos a entregar, pero mantendremos una estrategia interna de sistematización de la experiencia.

##6. Plasmar la pertinencia de éste proyecto con el Software Público. 
Para esto haremos un cotejamiento entre los criterios establecidos por el SNSP para considerar una aplicación como Software Público:

* **Interés Público:** Orientadas a una gestión pública Eficiente.

El PRO Nacional busca establecer una plataforma base (framework) junto con una serie de módulos transversales para toda la Administración Pública (Planificación y Presupuesto, Contabilidad y Tesorería, Almacenes, Talento Humano). Está pensado para ser implantado en más de cuatro mil (4.000) instituciones públicas, las cuales están distribuidas en más de treinta mil (30.000) oficinas.

* **Disponibilidad de la Solución:** Es Software Libre y se encuentra a la disposición del público.

La solución final estará disponible en la plataforma que el CNTI disponga para tal fin. Sin embargo, es parte de nuestro quehacer cotidiano el uso de herramientas colaborativas tales como GitHub o GitLab para el control de versiones y la centralización de los archivos en estos repositorios. Pueden constatarlo ingresando a http://www.github.com/BachacoVE

* **Validado Y Recomendado:** Realiza las funciones para lo cual fue creado, es Recomendado. Cumple con la evaluación técnica (seguridad y calidad), es Certificado.

Actualmente estamos trabajando junto con Suscerte y CNTI en las validaciones tanto de Seguridad como de Calidad y Funcionalidad de la plataforma de desarrollo Odoo, la cual nos sirve como Framework de desarrollo. Aspiramos que dentro de pocas semanas obtengamos las certificaciones.

* **Mecanismo De Apropiación Del Conocimiento:** Documentación y 
formación. Se deberá realizar transferencia tecnológica tanto a los usuarios como a los prestadores de servicio

En este respecto podemos decir que la Comunidad BachacoVE viene formando desde su nacimiento hace dos años, por lo que hemos acumulado experiencia y material formativo suficiente como para acometer la construcción de un plan formativo con relativa rapidez. Sin embargo, dentro de los objetivos del proyecto establecemos la prioridad de la plataforma formativa masiva en linea abierta para elevar al máximo nuestras capacidades formativas. Es decir, la apropiación del conocimiento es un elemento transversal en el proyecto.

* **Soporte Técnico:**  Unidades Productivas, Comunidades y Empresas conforman un sistema de soporte y sustentabilidad de la solución
He aquí el punto de mayor importancia dentro del proyecto.

Constituir el Ecosistema de Unidades Productivas que ofrezca la implantación y brinde el soporte post-implantación del sistema y/o módulo, pasa inevitablmente por la acción sostenida del Estado en el impulso de las Unidades Productivas que se vinculen al desarrollo del PRO Nacional. Esto no sucederá mágicamente, ni podrán solas aquellas personas con la voluntad de organizarse productivamente, al menos no en la cantidad que se requiere. Los costos para iniciar una unidad productiva son variados, desde los aranceles del registro, pasando por personal necesario hasta el local de co-trabajo, son costos que alguien que sobrevive con su sueldo no puede asumir. Hace falta el apalancamiento. Todo esto está contemplado en el proyecto.

* **Comunidades De Usuarios:** Interacción de los diferentes actores a través de un medio virtual de trabajo colaborativo

La comunidad de usuarios está garantizada si se establece una política de vinculación inter-institucional. Sin embargo, tenemos contemplado acercarnos durante todo el proceso del proyecto con las instituciones, apartando un cupo importante de las formaciones técnicas para los equipos de las direcciones de tecnología, las cuales pasarán a formar parte de la red de soporte (funcional y técnica) posterior.

* **Accesibilidad:** Disponer de las condiciones de soporte y canales de información para garantizar el acceso a las personas con vulnerabilidad.

Asumimos este criterio como un elemento de honor para nuestra comunidad y vamos a cumplir con todas las normas y/o recomendaciones emanadas al respecto.

* **Licenciamiento:** Bajo las condiciones de la legislación venezolana.

Se prevé liberar el software resultante bajo la licencia AGPLv3, sin embargo es un elemento a coordinar con el CNTI para cumplir con sus recomendaciones.

Para un análisis más detallado, pueden consultar nuestro artículo Vinculación del Sistema Nacional de Software Público y el PRO Nacional ( www.bachaco.org.ve/?p=340 )


#Respuesta a observaciones de la Estructura de costos

##1. Recuerden que la estructura de Costos debe ir cónsona con el instrumento productivo, tienen campos en blanco, revisar el anexo.

ya los campos fueron llenados

##2. ¿Dónde están los costos de la conformación de la EPS?

las 5 unidades productivas de arranque del escenario mínimo ya se encuentran  conformadas legalmente.**

##3. ¿Cómo determinaron los clientes potenciales?

Varias de las instituciones listadas nos han contactado para contratar desarrollos particulares, hemos observado que los problemas que tratan de resolver con estos desarrollos son esencialmente los mismos en todas las instituciones, al no contar con la infraestructura para asumir varios proyectos individuales, nuestra propuesta es crear una solución única con el financimiento obtenido por el proyecto para que de forma mas efeciente tener la capacidad de desarrollar una única solución que cubra las necesidades de todos.

##4. En la totalidad del proyecto,  ¿qué tipo personal se contratará?, Cómo se decidió el monto a pagar?, ¿Qué tipo de asesoría recibirán?, ¿Cómo serán las contrataciones?, porque dependiendo del tipo de contratación, se puede excluir de los costos el pago de HCM.

El personal de las Unidades productivas siempre sera personal fijo de estas unidades atendiendo a las particularidades de las figuras jurídicas de cada unidad productiva. Este sería el nucleo base del ecosistema el cual debe contar con los beneficios necesarios para su estabilidad. El monto a pagar del personal se definio como la mitad del sueldo promedio que cobran profesionales por trabajo similar en el mercado privado

Las asesorías sí son aparte, por contrataciones por honorarios profesionales y no contemplan pago de HCM, 

##5. Los gastos de uniformes aun continúan en gastos de personal, estos costos corresponden a la parte administrativa.

Debido a la necesidad de reducir al máximo los costos, los gastos de uniformes fueron removidos.

##6. En los gastos de materia prima, ¿qué se contempla como materia prima, que tipo de materiales y suministros?

Los materiales  y suministros contemplan productos como pen drives, papel, consumibles de impresoras, talonarios de facturación, cables, cd's, dvd's y afines

##7. ¿Cómo determinaron los 300.000, 00Bs de imprevistos?
 
Fueron actualizados a 50.000 por mes. E calculo que utilizamos es que cubriera al menos el 20% del sueldo de un trabajador

##8. Los gastos de promoción y comercialización solo están contemplados en la parte de formación y capacitación, el resto de los productos ¿cómo lo mercadearan?

Al tener un mercado cautivo y tantos clientes en cola a los cuales no le hemos podido brindar servicios por nuestra temporal falta de capacidad de respuesta, solo vemos necesaria la comercialización en el sentido de captar talento humano para las up mediante la captacion y formación y asi aumentar de forma sostenible nuestra capacidad de respuesta

##9. ¿A qué se debe la solicitud de 60 computadoras y 60 laptos? Cuántas personas las utilizarán

En principio se propuso que cada miembro de la unidad productiva contara con una computadora de escritorio para utilizarla en los espacios de trabajo y una laptop que pudiera movilizar cuando necesitara trabajar remotamente o estando en sitio con los clientes haciendo implementaciones, para reducir costos hemos reducido el numero de computadoras a solo 1 laptop por persona.

##10. En general  toda la estructura de costos se plantea de manera casi homogenea, en especial en los costos de fabricación y producción, ¿cómo se explica que sea el mismo costo de servicios básicos para todos los bienes y servicios y en general ocurre con gran parte de los costos?.

Como el plantamiento inicial es que las unidades productivas trabajen en un ambiente de co-trabajo y los costos de frabricacion y produccion son esencialmente los mismos para todos los productos al ser productos intelectuales de Tecnologias de la información lo que se hizo fue dividir proporcionalmente el costo total de fabricación de cada producto con respecto a la cantidad de personas que trabajaran en cada uno de estos

##11. En la estructura de ingresos se dice que realizarán 600 cursos en un año, tomando en cuenta que el proyecto no cuenta con una infraestructura establecida, ¿éste número de cursos es real? Se debe destinar una parte a explicar detalladamente la actividad de formación y su logística.

En un principio planteamos solo elaborar los contenidos de formación y montarlos en la plataforma de formación del CNTI, pero para reducir costes del proyecto hemos suprimido tambien de los productos la elabroación de los contenidos, al ser mas prioritaria el desarrollo del PRO

##12. La proyección de la utilidad neta mensual y anual, debe ser revisada, tomando en cuenta la deficiencia que hay en el cálculo de los costos y el desconocimiento de los patrones que se han tomado para establecer los precios de los bienes y servicios.

Revisada y actualizada. En el mercado internacional, la hora de soporte, desarrollo, o servicios asociados a las tecnologías de información está entre los 15$ y 30$ por horas. En la calle, esto se calcula en base a Bs 1.000 por cada dolar. Sin embargo, consideramos que nuestros costos iniciales puedes oscilar entre 5.000 y 10.000 por hora, de manera que podamos ser competitivos establecer una cartera clientes rápidamente.

El primer año de operaciones lo cubriríamos con el financiamiento. El segundo año ya iniciaríamos con el cobro de servicios. Sin embargo, al finalizar el primer año ya estaríamos entregando productos al Estado. Es decir, no es un préstamo. Es un aporte para fortalecer la industria y recibir un producto a cambio. No consideramos que este financiamiento deba ser retornable, dado que resolveremos una necesidad transversal del Estado.
