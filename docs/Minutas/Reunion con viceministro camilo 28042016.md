# OBSERVACIONES DE KENNY SURGIDAS EN LA REUNIÓN DEL MIÉRCOLES 27 DE ABRIL EN EL CNTI

___

+ Llenar formato del MPPEUCT
+ Quitar los costos relaciones a la implantación de la plataforma formativa, excepto a la compra de los hierros
+ Quitar dos módulos de la formación que asumirá Reacciun (Ley de Infogobierno y Desarrollo Colaborativo)
+ Incluir las instituciones en las cuales se ha implementado OpenERP/Odoo
+ Incluir que el proyecto PRO se está haciendo en sinergia con el Proyecto de Software Público del CNTI+ +
+ Incluir explicación sobre qué significa “Actores”
+ Incluir gráficos de: Cierre de Ciclo, indicadores de la CEPAL y Software Público
+ ¿Cuanto se gasta en dolares por SAP entre CVG, CANTV y PDVSA

___

# PREGUNTAS SURGIDAS DELA REUNIÓN DEL JUEVES 28 CON EL VICEMINISTRO CAMILO TORRES Y SU EQUIPO, EN RELACIÓN AL PROYECTO SISTEMA PRO NACIONAL. REALIZADA EN EL CNTI

___


## Sugerencias Generales: [mias o ajenas]

1. Llenar el formato sugerido por el MPPEUCT (Indira)(Viabilidad Económica)
2. Hacer una ficha resumén de 1 o 2 páginas
3. hacer una presentación ODP [con más gráficas]
4. Camilo: Elevar [subir el nivel de] el nombre del proyecto. Que no nos limite el nombre
5. Hacer una especial atención en el Ecosistema Productivo
6. Son importantes los números [en relación al impacto]
7. Gráficas de Sustentabilidad, Inversión y Productos
8. Enfatizar el modelo de negocios
1. Contrato de Mantenimiento y Soporte
2. Desarrollo de Módulos Verticales
3. Formación Técnica y Funcional
4. Reinserción de Excedentes
9. Enfatizar el teletrabajo
10. Enfatizar la estrategia de concreción de este proyecto (mostrar el proyecto dividido en sus tres fases)
11. Software Libre no es Gratis, esforzarse por dejar claro que SÍ se va a cobrar, desestigmatizar el SL como gratuidad
12. Destacar elementos estratégicos del Proyecto (por ejemplo: sueldos por 12 meses, equipos, etc)
13. Camilo: Apostar a la eficiencia de las instituciones
14. Camilo: El texto en el proyecto captura el espíritu de lo que se debe hacer pero no materializa la idea de lo que se debe hacer
15. Camilo: Reorientar las ideas [La idea está allí, solo que mal organizada]
16. Camilo: Sugerencia de Nombre: ESTRATEGIA DE SUSTENTABILIDAD PARA EL DESARROLLO DE SOFTWARE LIBRE EN VENEZUELA
17. Agregar Glosario de Términos

## Ana Karina Simmons

+ ¿Cuales son los mecanismos [de ejecución del proyecto]?
+ ¿Cómo se mantiene?
+ ¿Que se quiere decir con “malla curricular?
+ En relación a los espacios, ¿solo los Infocentros? ¿Tienen que ser donados? O ¿ pueden ser prestados?
+ ¿Cómo [o cual] es la viabilidad?
+ ¿Cómo es el modelo de negocios [para la sustentabilidad]?
+ ¿Pueden especificar cuanto se gasta en Software en el Estado?
+ No se ve el cómo se va a construir. Desglose de fases. ¿Cuál es la metodología?
¿Cómo es el plan de Inversión? [Por pasos]
Indicadores: ¿Cuales son? ¿Cómo vamos a medir lo que vamos haciendo?
Definir Temas Legales, Alianzas, Cadena Productiva de Comercialización [Cadena de Valor]
Agregar Limitantes
¿Donde están las células iniciales del Ecosistema Productivo?

## Camilo Torres

+ En cuanto a la formación Académica, ¿saben bien cómo [van a incidir en las universidades]?
+ ¿que quieren decir con Contrato Social?
+ Donde y en qué contexto aplica un tipo de contrato [social, negocio]?
+ ¿Cual es el alcance del proyecto?
+ ¿Cuantas personas asociadas al arranque?
+ ¿Donde se ve el límite entre el PRO y el Ecosistema?
+ ¿Qué es el Ecosistema Productivo? [¿Esto incluye a todos los actores del ecosistema?] Si es solo TIL no debería llamarse así simplemente.
+ ¿Cómo se direcciona el contenido en las universidades?
+ ¿Cómo se hace tangible la economía digital?


## Indira

+ ¿Cuales son los antecedentes de este proyecto?
+ Hacer énfasis en la industria TIL
+ ¿Qué estrategias plantean para vencer el muro de información típico en las instituciones?
+ [Sobre las fases] ¿que va primero, la movilización, los insumos, o qué?

## Desconocido

+ ¿Cómo se contempla la realización de las fases?

## Judith

+ ¿Cómo es el Retorno de Inversión?
+ Cómo garantizamos un porcentaje de deserción? [cual es la estrategia]
+ ¿Cuanto se va a ahorrar el estado en Divisas?






Próxima reunión el Miércoles 4 de Mayo a las 3pm
Taller el 5 de Mayo para revisar modelos productivos. [Proponer telepresencia]


# Sugerencias para mecanismos:

Elementos Centrales:

1. Modelo de Negocio:

    + Viabilidad
    + Productos ó Servicios
	
2. Estrategia de desarrollo del Proyecto

    + Quienes
	+ Como
	+ En cuanto tiempo
	+ Qué necesitan
	
3. Vinculación con las Universidades


Todos los recursos lo mantiene a nombre de la fundación, hasta que se cumplan los compromisos.

# Respuestas:

2.- ** ¿Cómo se mantiene?** De la implantación, del mantenimiento, del soporte, de los nuevos requerimientos, de la formación (funcional o técnica), desarrollo en otras tecnologías (móvil, web, etc)

3.- ** ¿Que se quiere decir con “malla curricular"? ** Se refiere al contenido programático que deberían desarrollar las universidades para desarrollar las capacidades de los estudiantes en dirección a la tecnología implicada en el PRO-Nacional

4.- ** En relación a los espacios, ¿solo los Infocentros? ¿Tienen que ser donados? O ¿ pueden ser prestados? ** Como sea, tomando en cuenta la sustentabilidad en el tiempo
5.- ¿Cómo [o cual] es la viabilidad? Explícate

6.- ** ¿Cómo es el modelo de negocios [para la sustentabilidad]? ** Igual al punto 2 El Estado paga el desarrollo una vez. el estado lo asume transversalmente y nosotros ofrecemos el servicio post-desarrollo.

7.- ** ¿Pueden especificar cuanto se gasta en Software en el Estado? ** No hay quien dé esta cifra

8.- ** No se ve el cómo se va a construir. Desglose de fases. ¿Cuál es la metodología? ** Revisar los documentos de construcción de cada componente del proyecto.

9.- ** ¿Cómo es el plan de Inversión? [Por pasos] ** Mostrar las estructuras de costos

10.- ** Definir Temas Legales, Alianzas, Cadena Productiva de Comercialización [Cadena de Valor]. ** Mostrar las presentaciones de la INTIL. Pag 14 del archivo Entregable_Consolidado_V15_Imprimible.pdf

11.- ** Indicadores: ¿Cuales son? ¿Cómo vamos a medir lo que vamos haciendo?. **  Deberían salir de los productos

12.- ** Donde están las células iniciales del Ecosistema Productivo? ** Cooperativa Juventud Productiva Venezolana, Ávalon Consultores, Orión, Codex, Cooperativa Sarí, y Empero.

13.- *** En cuanto a la formación Académica, ¿saben bien cómo [van a incidir en las universidades]? ** Se destinarán recursos y gente a abordar las universidades con charlas y talleres para captar talentos y generas grupos de interés dentro las universidades. Luego se realizarán talleres especializados. Mostrar el triángulo del proyecto

14.- ** ¿que quieren decir con Contrato Social? Donde y en qué contexto aplica un tipo de contrato [social, negocio]? ** Un contrato social ( Estatutos)se entiendo por un acuerdo interno de una comunidad específica que se compromete a cumplir dichos acuerdos generalmente éticos y morales, de interacción interno. El contrato social se establece como la basa para generar la armonía de los participantes.

15.- ** ¿Cual es el alcance del proyecto? ** El proyecto concluye al: a) hacer la prueba de implementación piloto en la institución seleccionada de los módulos propuestos. b) Al conformar las 20 unidades productivas establecidas legalmente, en completa sinergia para ofrecer los servicios asociados al post-desarrollo (modelo de negocios) junto con un modelo de cómo conformar un ecosistema productivo alrededor de una tecnología. c) Al tener una plataforma formativa masiva a distancia con los 13 contenidos planteados.

16.- ** ¿Qué es el Ecosistema Productivo? [¿Esto incluye a todos los actores del ecosistema?] Si es solo TIL no debería llamarse así simplemente. ** El nombre propuesto es: Conformación del Ecosistema Productivo de Tecnologías Libres para el PRO-Nacional. Debemos tener cuidado en pretender abarcar todo el sector TIL ya que es muy ámplio y podriamos generar expectativas incorrectas.

17.- ** ¿Cómo se direcciona el contenido en las universidades? ** A través del Pensum, maestría, postgrados, diplomados, etc.

18.- ** ¿Qué estrategias plantean para vencer el muro de información típico en las instituciones? ** Esto es un tema de gobierno que debe trabajarse en el camino. Al tener el producto inicial y probar que funciona, la necesidad en las instituciones abrirá muchas puertas, pero una política fuerte emanada de los entes rectores es estratégico.
