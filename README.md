## ¿Qué es el PRO Nacional?


Según el Censo Económico de 2007-2008, en Venezuela existen más de 30.000 oficinas y/o instituciones públicas y según datos no oficiales de la Comisión Nacional de las Tecnologías de Información (CONATI) son alrededor de 4.000 organismos públicos distintos, que varían en tamaño y complejidad; y según lo establecido en el Artículo 34° de la Ley de Infogobierno el Estado "sólo empleará programas informáticos en software libre y
estándares abiertos", pero la capacidad real actual de acometer esta migración es mucho menor a la necesaria; es por esto que se hace urgente una estrategia asimétrica para atenderlas.

Es muy grave encontrar que, aunque esas 30.000+ instituciones pertenecen a un mismo Estado, tienen una estrategia de desarrollo de sus aplicaciones aislada unas de otras, pudiendo establecer acuerdos de intercambio o unir recursos y talentos para desarrollar sus sistemas comunes.

Entre las soluciones libres que existen actualmente, ninguna presenta un enfoque integral e integrado para los procesos de las Instituciones Públicas. Estas soluciones se quedan alrededor de la gestión administrativa y no son extensibles para soportar los demás procesos operativos particulares de cada institucion, como por ejemplo Ventas, Facturación, Inventario, Gestión de Fabricación así como otros capacidades indispensables en la actualidad tales como Inteligencia sobre los datos, Georeferencación, Interoperabilidad.

El PRO Nacional, como sistema ERP Libre, desarrollado con metodologías Colaborativas y adaptado a la realidad venezolana permitiría satisfacer todas estas limitantes a la par que desarrollaría un Ecosistema de Unidades Productivas basado en la asesoría para la implementación, además de las modificaciones y/o desarrollo de nuevos módulos, además del soporte y la formación sin tener que depender de una empresa con chequera abultada y nómina amplia. Permitiría fortalecer los modelos cooperativistas al poder apostar a las Pymes y así evitar el modelo explotador de las grandes casas de software. El PRO Nacional puede ser fácilmente implementable en las instancias del poder popular, bien sea en un consejo comunal o en la comuna para hacer una gestión eficiente de sus EPS. El Pro Nacional además, tiene mucha capacidad para atender las necesidades de la empresas productivas del Estado.

El ERP Nacional, al ser implementado masivamente en el Poder Público, potenciará las capacidades de control y seguimiento de la gestión pública, lo que permitirá a los decisores tener una visión holística de todo el proceso, sus puntos débiles, con alertas tempranas para la toma de decisiones.

Acometer el desarrollo del PRO Nacional permitiría desarrollar un sector hasta ahora invisibilizado y subestimado como es el de desarrollo de software, que junto a otros componentes de la economía digital – telecomunicaciones (47%), servicios TIC (24%), dispositivos (17%), software (8%) y centros de datos (4%) –  en otros países de América Latina ha alcanzado hasta el 5% del PIB (CEPAL 2013).

Para acometer el desarrollo del PRO Nacional nos apoyamos en la plataforma de Software Libre Odoo, la cual lleva más de 10 años de maduración y cuenta con una ámplia comunidad de desarrolladores alrededor del mundo y en Venezuela. Además cuenta con un framework de desarrollo adaptado a estándares abiertos ampliamente utilizados y una serie de módulos preconstruidos que adelantan y reducen en gran medida los tiempos de desarrollo. Esta plataforma cuenta con características de Interoperabilidad lo que permitirá avanzar rápidamente hacia el Gobierno en Línea y reducir a casi “tiempo real” el reporte de información de las instituciones para una toma de decisiones más acertada y oportuna.

PRO significa Planificador de Recursos Organizacionales, y es una adaptación a nuestro idioma del término anglosajón ERP (Enterprise Resource Planning)

Un sistema de Planificación de Recursos Empresariales (por sus siglas en inglés ERP) es un paquete de aplicaciones que son integradas para asistir a una organización en la recolección, gestión y reporte de información de todos los procesos medulares. Estas aplicaciones, generalmente llamadas módulos, pueden ser instaladas y configuradas independientemente, adaptadas a las necesidades específicas de la organización. Este diseño modular de la mayoría de los ERP ofrece a las organizaciones una gran flexibilidad para implementar el sistema.

## ¿Qué modulos deben conformar el PRO Nacional?
* Planificación
* Presupuesto
* Contabilidad
* Finanzas
* Talento Humano
* Inventario
* Bienes Públicos
* Georeferenciación

## ¿Cómo puedo contribuir con el PRO Nacional?

**Importante:** Todas las propuestas deben hacerse a través de Issues en este repositorio. [Ver issues](https://gitlab.com/vijoin/Proyecto-PRO-Nacional/issues)

### General
* Sugiriendo módulos [Ver módulos propuestos](addons/README.md). Ver también [módulos propuestos por JPV](docs/Documentos_Argumentativos/Vértice PRO Nacional/que-deberia-llevar-el-PRO.odt)
* Compartiendo manuales, leyes o cualquier otro elemento de documentación de procesos de la gestión pública
* Validando procesos
* Probando los módulos y detectando errores



### Como Unidad Productiva
* Desarrollo
* Implementación
* Formación
* Soporte

### Como Usuario de Pruebas
* Funcionario de Institución Pública

### Donaciones

## ¿Quienes son y donde están ubicadas las personas comprometidas con el PRO Nacional?
* Fundación ATTA (Caracas, Venezuela)
* Colectivo BachacoVE (Caracas, Venezuela)
* Consultores Codex Tecnología C.A (Caracas, Venezuela)
* Consultores Tecnológicos Jeikasoft C.A (Caracas, Venezuela)
* Empero C.A (Caracas, Venezuela)
* Asociación Cooperativa Juventud Productiva Venezolana (Caracas, Venezuela)
* Consultores Tecnológicos Albatech C.A (Caracas, Venezuela)
* Open Knowledge Consultores, C.A (Caracas, Venezuela)
* Mariángela Petrizzo (Mérida, Venezuela)
* Francisco Palm (Mérida, Venezuela) 

## ¿Qué Instituciones Públicas Contribuyen en este Desarrollo?
* MPPEUCT
* MPPP
* CONATI
* CNTI
* SNC

## Otros Vértices del Proyecto
![docs/Documentos_Argumentativos/Graficos/vertices.png](docs/Documentos_Argumentativos/Graficos/vertices-peq.png)

## Metodología a utilizar

Utilizaremos la Metodología de Desarrollo Colaborativo de Software Libre recomendada por CENDITEL [MDCSL-MetodologiaSoftwareLibre.pdf](docs/Documentos_Metodologicos/MDCSL-CENDITEL-Metodologia_Desarrollo_Colaborativo_de_Software_Libre/MDCSL-MetodologiaSoftwareLibre.pdf)

Mas información Revisar la ruta: [Documentos Metológicos](docs/Documentos_Metodologicos/)